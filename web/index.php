<?php
require_once 'php/lang.php';
require_once 'php/db.php';
?>
<!doctype html>
<html lang="<?= $lang ?>">
<?php require_once 'php/head.php' ?>
<body class="index-page">

<?php require_once 'php/header.php' ?>

<section id="banner-section">
    <div class="carousel">
        <div class="slide-box">
            <div class="slide main-slide">
                <div class="container">
                    <div class="title"><?= tr('ПРОСТІР СЬОГОДНІ') ?></div>
                    <div class="box">
                        <div class="item">
                            <img src="/img/main-banner/bank.svg">
                            <div class="count" data-count-target="50">0</div>
                            <div class="count-title"><?= tr('УЧАСНИКІВ') ?></div>
                        </div>
                        <div class="item">
                            <img src="/img/main-banner/terminal.svg">
                            <div class="count" data-count-target="236228">0</div>
                            <div class="count-title"><?= tr('ТЕРМІНАЛІВ') ?></div>
                        </div>
                        <div class="item">
                            <img src="/img/main-banner/atm.svg">
                            <div class="count" data-count-target="15275">0</div>
                            <div class="count-title"><?= tr('БАНКОМАТІВ') ?></div>
                        </div>
                        <div class="item">
                            <img src="/img/main-banner/ptks.svg">
                            <div class="count" data-count-target="16314">0</div>
                            <div class="count-title"><?= tr('ПТКС') ?></div>
                        </div>
                        <div class="item">
                            <img src="/img/main-banner/wallet.svg">
                            <div class="count" data-count-target="972">0</div>
                            <div class="count-title"><?= tr('ДЕПОЗИТНИХ БАНКОМАТІВ') ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php foreach($banners as $item): ?>
                <div class="slide" style="background: url('<?= $item ['img'] ?>') center top / cover no-repeat;">
                    <div class="text"><?= $item ['text'][$lang] ?></div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="xlSNext"></div>
        <div class="xlSPrev"></div>
    </div>
</section>

<section id="news-section">
    <h2><?= tr('Новини') ?></h2>
    <div class="container">
        <div class="news-slider-box">
            <div class="news-slider">
                <?php foreach($news as $item): ?>
                    <div class="news-slider-item-box">
                        <a href="/news-item.php?id=<?= $item['id'] ?>" class="news-slider-item">
                            <div class="new-slider-item-img" style="background-image:url(<?= $item['img'] ?>);"></div>
                            <div class="new-slider-item-text-box">
                                <div class="new-slider-item-date"><?= $item['date'] ?></div>
                                <div class="new-slider-item-title"><?= $item['title'][$lang] ?></div>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="xlSNext"></div>
        <div class="xlSPrev"></div>
    </div>
</section>

<section id="partners-section">
    <h2><?= tr('Учасники простір') ?></h2>
    <h3><?= tr('Сьогодні учасниками ПРОСТІР є') ?></h3>
    <div class="container">
        <div class="partners-slider-box">
            <div class="partners-slider">
                <?php foreach($all_members as $item): ?>
                    <div class="partners-slider-item"
                         style="background-image:url(<?= $item['img'] ?>);" title="<?= $item['name'] ?>"></div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="xlSNext"></div>
        <div class="xlSPrev"></div>
    </div>
</section>

<?php require_once 'php/footer.php' ?>

</body>
</html>
