<?php
require_once 'php/lang.php';

?>
<!doctype html>
<html lang="<?= $lang ?>">
<?php require_once 'php/head.php' ?>
<body class="about-page">

<?php require_once 'php/header.php' ?>

<section class="banner">
    <div class="container">
        <h1><?= tr('Про Нас') ?></h1>
    </div>
</section>

<?php if($lang === 'en'): ?>
    <section class="content">
        <div class="container">
            <p>
                National payment system PROSTIR is a national system of retail payments wherein payments for goods
                and services, cash withdrawals, and other transactions are conducted using an electronic means of
                payment, namely PROSTIR payment cards
            </p>
            <p>
                NPS PROSTIR is developed by the National Bank of Ukraine. The full name of NPS PROSTIR is the National
                Payment System "Ukrainian Payment Area (Prostir)". Until its rebranding in 2016, NPS PROSTIR was known as
                National System of Mass Electronic Payments (NSMEP)
            </p>
            <h3>Mission</h3>
            <p>We facilitate cashless payments growth in Ukraine</p>
            <h3>Goals</h3>
            <div class="list">
                <div class="item">
                    <i class="icon-check"></i>
                    <span>To become a competitive and nation-wide payment system in Ukraine</span>
                </div>
                <div class="item">
                    <i class="icon-check"></i>
                    <span>To meet the actual needs of Ukrainians with a high-quality, state-of-the-art, and cost-effective payment instrument</span>
                </div>
                <div class="item">
                    <i class="icon-check"></i>
                    <span>To become a leader in implementing innovative payment solutions</span>
                </div>
            </div>
            <div class="link-box">
                <i class="icon-prostir"></i>
                <a href="http://prostir.gov.ua/prostir/brand_book" target="_blank">Trademark PROSTIR</a>
            </div>
        </div>
    </section>
<?php else: ?>
    <section class="content">
        <div class="container">
            <p>
                Національна платіжна система ПРОСТІР – це система роздрібних платежів, у якій розрахунки за товари та послуги,
                отримання готівки та інші операції з національною валютою здійснюються за допомогою електронних платіжних засобів,
                а саме платіжних карток ПРОСТІР
            </p>
            <p>
                Платіжна система ПРОСТІР створена Національним банком України. Повна назва системи ПРОСТІР – Національна
                платіжна система «Український платіжний простір». До ребрендингу, проведеного у 2016 році, платіжна
                система ПРОСТІР мала назву Національна система масових електронних платежів (скорочено НСМЕП)
            </p>
            <h3>Місія</h3>
            <p>Сприяти зростанню рівня роздрібних безготівкових розрахунків в Україні</p>
            <h3>Цілі</h3>
            <div class="list">
                <div class="item">
                    <i class="icon-check"></i>
                    <span>Стати конкурентною та справді Національною платіжною системою в Україні</span>
                </div>
                <div class="item">
                    <i class="icon-check"></i>
                    <span>Відповідати актуальним потребам українських громадян у отриманні якісного, сучасного та економного платіжного інструменту</span>
                </div>
                <div class="item">
                    <i class="icon-check"></i>
                    <span>Стати лідером у впровадженні інноваційних платіжних рішень</span>
                </div>
            </div>
            <div class="link-box">
                <i class="icon-prostir"></i>
                <a href="http://prostir.gov.ua/prostir/brand_book" target="_blank">Торгівельна марка Простір</a>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php require_once 'php/footer.php' ?>

</body>
</html>
