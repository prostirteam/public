<?php
require_once 'php/lang.php';

?>
<!doctype html>
<html lang="<?= $lang ?>">
<?php require_once 'php/head.php' ?>
<body class="business-page">

<?php require_once 'php/header.php' ?>

<section class="banner">
    <div class="container">
        <h1><?= tr('Бізнесу') ?></h1>
    </div>
</section>

<?php if($lang === 'en'): ?>
    <section class="content">
        <div class="container">
            <div class="line-box">
                <h3>Is it profitable to acquire PROSTIR cards for payments?</h3>
                <p>
                    PROSTIR payment system provides banks with an economically justified opportunity to lower tariffs
                    for trade and service enterprises. For more information on tariffs for acquiring PROSTIR cards,
                    please refer to your bank or PROSTIR member banks
                </p>
            </div>
            <div class="line-box">
                <h3>What should be done to install a POS-terminal for PROSTIR cards?</h3>
                <p>
                    To install a POS-terminal for PROSTIR cards, please apply to a PROSTIR member bank and sign a
                    contract with it for provision of acquiring services
                </p>
            </div>
            <div class="line-box">
                <h3>Can the same POS-terminal service VISA, MASTERCARD, and PROSTIR cards at the same time?</h3>
                <p>
                    The same POS-terminal can service PROSTIR, MasterCard, and Visa cards at the same time. PROSTIR
                    is created according to open standards, on which the majority of payment systems are based,
                    including those operating in Ukraine
                </p>
            </div>
        </div>
    </section>
<?php else: ?>
    <section class="content">
        <div class="container">
            <div class="line-box">
                <h3>Чи вигідно приймати картки ПРОСТІР до оплати</h3>
                <p>
                    Платіжна система ПРОСТІР надає українським банкам економічно обґрунтовану можливість знизити тарифи
                    на обслуговування торговельно-сервісних підприємств. Детальну інформацію щодо тарифів на еквайринг
                    карток ПРОСТІР Ви можете дізнатись у представників банку, в якому Ви обслуговуєтесь, або звернувшись
                    до банків-учасників ПРОСТІР
                </p>
            </div>
            <div class="line-box">
                <h3>Що треба зробити, щоб встановити POS-термінал для приймання карток ПРОСТІР</h3>
                <p>
                    Для встановлення POS-термінала, який обслуговує картки ПРОСТІР, потрібно звернутися до одного з
                    <a href="/members.php">банків-учасників</a>
                    ПРОСТІР та укласти з ним договір про надання послуг еквайрингу
                </p>
            </div>
            <div class="line-box">
                <h3>Чи може POS-термінал обслуговувати одночасно картки ПРОСТІР, Visa та MasterCard</h3>
                <p>
                    Один і той самий POS-термінал може одночасно обслуговувати картки платіжних систем ПРОСТІР,
                    Visa та MasterCard. Національна платіжна система ПРОСТІР побудована з використанням відкритих
                    стандартів, на яких базується більшість міжнародних платіжних систем, у тому числі, які працюють в Україні
                </p>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php require_once 'php/footer.php' ?>

</body>
</html>
