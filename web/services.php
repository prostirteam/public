<?php
require_once 'php/lang.php';
require_once 'php/db.php';
?>
<!doctype html>
<html lang="<?= $lang ?>">
<?php require_once 'php/head.php' ?>
<body class="services-page">

<?php require_once 'php/header.php' ?>

<section class="banner">
    <div class="container">
        <h1><?= tr('Сервіси') ?></h1>
    </div>
</section>

<section class="content">
    <div class="container">
        <div class="services-box">
            <?php foreach($services as $item): ?>
                <div class="services-item-box">
                    <div class="services-item">
                        <div class="services-item-img" style="background-image:url(<?= $item['img'] ?>);"></div>
                        <div class="services-item-title"><?= $item['title'][$lang] ?></div>
                        <div class="services-item-text"><?= $item['text'][$lang] ?></div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <h3><?= tr('services-page-text-1') ?></h3>
        <div class="xlSNext"></div>
        <div class="xlSPrev"></div>
    </div>
</section>

<?php require_once 'php/footer.php' ?>

</body>
</html>
