<?php
require_once 'php/lang.php';

?>
<!doctype html>
<html lang="<?= $lang ?>">
<?php require_once 'php/head.php' ?>
<body class="banks-page">

<?php require_once 'php/header.php' ?>

<section class="banner">
    <div class="container">
        <h1><?= tr('Банкам') ?></h1>
    </div>
</section>

<?php if($lang === 'en'): ?>
    <section class="content">
        <div class="container">
            <div class="advantages-box">
                <div class="advantages-item-box">
                    <div class="advantages-item">
                        <div class="advantages-item-img" style="background-image:url(/img/banks-page-icons/1.svg);"></div>
                        <div class="advantages-item-title">Why PROSTIR?</div>
                        <div class="advantages-item-text">
                            <div class="list">
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        97.7 % of all transactions with cards issued by Ukrainian banks are
                                        performed within the country, and only 2.3 % abroad
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        PROSTIR is an EMV CPA-based system. ATMs and terminals are fully compatible
                                        with international payment systems` technical standards
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        92 % of POS-terminals and 83 % ATMs can already process PROSTIR payment cards
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        Internet payments and settlements can be done with PROSTIR cards
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="advantages-item-box">
                    <div class="advantages-item">
                        <div class="advantages-item-img" style="background-image:url(/img/banks-page-icons/2.svg);"></div>
                        <div class="advantages-item-title">We offer cost-efficiency</div>
                        <div class="advantages-item-text">
                            <div class="list">
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        Low entrance fee and quick connection to PROSTIR
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        System fees are several times lower in PROSTIR than in international systems
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        A much lower security deposit
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        No extra charges for payment cards issued
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        No charge for opening/maintaining accounts with a settlement bank
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        The possibility to receive issuer and acquirer status (including Internet-acquirer one) with no additional charges
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="advantages-item-box">
                    <div class="advantages-item">
                        <div class="advantages-item-img" style="background-image:url(/img/banks-page-icons/3.svg);"></div>
                        <div class="advantages-item-title">We offer security</div>
                        <div class="advantages-item-text">
                            <div class="list">
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        No FX risks, as the settlement currency in PROSTIR is the Ukrainian hryvnia.
                                        Contributions to PROSTIR`s security fund are also made in hryvnia
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        PROSTIR is a local payment system, which implies a low level of transaction
                                        fraud, as most fraudulent transactions are registered in countries other than
                                        the payment card`s issuing country. A modern dispute resolution system is
                                        implemented in PROSTIR
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="advantages-item-box">
                    <div class="advantages-item">
                        <div class="advantages-item-img" style="background-image:url(/img/banks-page-icons/4.svg);"></div>
                        <div class="advantages-item-title">More possibilities with PROSTIR</div>
                        <div class="advantages-item-text">
                            <div class="list">
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        Membership in PROSTIR allows a bank to participate in an open tender for
                                        an authorized bank for payment of salaries to public sector employees,
                                        public welfare payments, etc
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        Availability of "PROSTIR e-money". Members of PROSTIR don`t need to register
                                        their own e-money system rules to be able to issue it; the authorization
                                        procedure is much simplified and flexible
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class="headline">
                The procedure for joining PROSTIR can be found at
                link.
                <a href="https://bank.gov.ua/control/uk/publish/article?art_id=47103&cat_id=46474">link</a>. <br>
                Please, address all questions about specifics
                of PROSTIR membership to the PROSTIR service desk by phone +38 044 527 3958 or e-mail: PROSTIR@bank.gov.ua
            </p>
        </div>
    </section>
<?php else: ?>
    <section class="content">
        <div class="container">
            <div class="advantages-box">
                <div class="advantages-item-box">
                    <div class="advantages-item">
                        <div class="advantages-item-img" style="background-image:url(/img/banks-page-icons/1.svg);"></div>
                        <div class="advantages-item-title">Чому ПРОСТІР?</div>
                        <div class="advantages-item-text">
                            <div class="list">
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        97.7 % усіх операцій за картками українських банків-емітентів здійснюються на
                                        території України, лише 2.3 % - за її межами
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        Система ПРОСТІР побудована за міжнародними стандартами (EMV CPA).
                                        Банкоматно-термінальне обладнання є повністю технічно сумісним із стандартами
                                        міжнародних платіжних систем
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        92 % POS-терміналів та 83 % банкоматів можуть обслуговувати картки ПРОСТІР
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        Картками ПРОСТІР можна здійснювати платежі та розрахунки в Інтернет
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="advantages-item-box">
                    <div class="advantages-item">
                        <div class="advantages-item-img" style="background-image:url(/img/banks-page-icons/2.svg);"></div>
                        <div class="advantages-item-title">З нами вигідніше</div>
                        <div class="advantages-item-text">
                            <div class="list">
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        ПРОСТІР гарантує низьку вартість вступу і швидке приєднання
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        Системні комісійні в ПРОСТІР у рази нижчі, ніж у міжнародних платіжних системах
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        Істотно менший розмір страхового депозиту
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        Плата за наявну емісію платіжних карток відсутня
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        Плата за відкриття/супроводження рахунків у розрахунковому банку відсутня
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        Існує можливість отримання статусів емітента та еквайра без додаткових плат
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="advantages-item-box">
                    <div class="advantages-item">
                        <div class="advantages-item-img" style="background-image:url(/img/banks-page-icons/3.svg);"></div>
                        <div class="advantages-item-title">З нами безпечно</div>
                        <div class="advantages-item-text">
                            <div class="list">
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        З системою ПРОСТІР повністю відсутні валютні ризики, оскільки розрахункова валюта ПРОСТІР – гривня.
                                        Внесок до страхового фонду ПРОСТІР також формується в гривнях
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        ПРОСТІР – локальна платіжна система, що передбачає низький рівень шахрайських
                                        операцій, адже основна кількість шахрайських операцій відбувається в країні
                                        відмінній ніж та, де були отримані дані платіжних карток.
                                        У ПРОСТІР запроваджена сучасна диспутна система
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="advantages-item-box">
                    <div class="advantages-item">
                        <div class="advantages-item-img" style="background-image:url(/img/banks-page-icons/4.svg);"></div>
                        <div class="advantages-item-title">Додаткові можливості з ПРОСТІР</div>
                        <div class="advantages-item-text">
                            <div class="list">
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        Вступ до ПРОСТІР дає змогу банку брати участь у відкритому конкурсі з вибору
                                        уповноваженого банку для виплати заробітної плати працівникам бюджетних установ,
                                        державної соціальної допомоги тощо
                                    </span>
                                </div>
                                <div class="item">
                                    <i class="icon-check"></i>
                                    <span>
                                        Наявність продукту «електронні гроші ПРОСТІР». Учаснику ПРОСТІР немає потреби
                                        проходити процедуру реєстрації власних правил системи електронних грошей для здійснення
                                        їх випуску, процес отримання дозволу значно спрощений та максимально гнучкий
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class="headline">
                Порядок вступу банку до платіжної системи ПРОСТІР викладений за
                <a href="https://bank.gov.ua/control/uk/publish/article?art_id=47103&cat_id=46474">посиланням</a>. <br>
                Усі відповіді щодо особливостей вступу і подальшої роботи в ПРОСТІР можна отримати
                у менеджерів по роботі з учасниками ПРОСТІР за тел. (044) 527-39-58, або
                за електронною адресою PROSTIR@bank.gov.ua
            </p>
        </div>
    </section>
<?php endif; ?>

<?php require_once 'php/footer.php' ?>

</body>
</html>
