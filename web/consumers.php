<?php
require_once 'php/db.php';
require_once 'php/lang.php';

?>
<!doctype html>
<html lang="<?= $lang ?>">
<?php require_once 'php/head.php' ?>
<body class="consumers-page">

<?php require_once 'php/header.php' ?>

<section class="banner">
    <div class="container">
        <h1><?= tr('Приватним особам') ?></h1>
    </div>
</section>

<section class="content">
    <div class="container">
        <div class="line-box">
            <h3><?= tr('consumers-page-title-1') ?></h3>
            <p><?= tr('consumers-page-text-1') ?></p>
        </div>
        <div class="line-box">
            <h3><?= tr('consumers-page-title-2') ?></h3>
            <p><?= tr('consumers-page-text-2') ?></p>
        </div>
        <div class="line-box">
            <h3><?= tr('consumers-page-title-3') ?></h3>
            <p><?= tr('consumers-page-text-3') ?></p>
        </div>
        <button class="order-card-btn"><?= tr('order-card-btn') ?></button>
        <div class="banks">
            <h3><?= tr('consumers-page-title-4') ?></h3>
            <h4><?= tr('consumers-page-title-5') ?></h4>
            <div class="members-box">
                <div class="members-slider">
                    <?php foreach($banks_in_consumers_page as $item): ?>
                        <a class="member-link" href="<?= $item['url'] ?>" target="_blank" title="<?= $item['name'] ?>"
                           style="background-image:url(<?= $item['img'] ?>);" data-bg-color="<?= $item['color'] ?>"></a>
                    <?php endforeach; ?>
                </div>
                <div class="xlSNext"></div>
                <div class="xlSPrev"></div>
            </div>
        </div>
    </div>
</section>

<div class="modal-box order-card-modal">
    <div class="modal">
        <div class="modal-header">
            <div class="modal-title"><?= tr('order-card-modal-title') ?></div>
            <div class="modal-close-icon">&times;</div>
        </div>
        <div class="modal-body">
            <div class="text"><?= tr('order-card-modal-text') ?></div>
            <div class="banks">
                <div class="bank">
                    <a class="bank-link" href="https://alfabank.ua/private-persons/pakety-uslug/prostirland?show=1" target="_blank" title="Альфа банк"
                       style="background-image:url(/img/banks/Alfabank.png);" data-bg-color="#f22e13"></a>
                    <div class="bank-tel">0 (800) 50 20 50</div>
                </div>
                <div class="bank">
                    <a class="bank-link" href="http://www.bankcenter.com.ua/promo/prostir/#order" target="_blank" title="Сenter bank"
                       style="background-image:url(/img/banks/center-bank.png);" data-bg-color="#2aafa8"></a>
                    <div class="bank-tel">(044) 393 74 00</div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once 'php/footer.php' ?>

</body>
</html>
