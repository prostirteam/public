<?php
require_once 'php/lang.php';
require_once 'php/db.php';

$news_id = (int)($_GET['id']) ?? null;
$news_item = null;
$url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

foreach ($news as $item) {
    if($item['id'] === $news_id) $news_item = $item;
}

if(!$news_item) {
    header('Location: /');
    exit;
}

?>
<!doctype html>
<html lang="<?= $lang ?>">
<?php require_once 'php/head.php' ?>
<body class="news-item-page">

<?php require_once 'php/header.php' ?>

<section class="breadcrumbs">
    <div class="container">
        <a href="/"><?= tr('Головна') ?></a>
        <span>&gt;</span>
        <a href="/news.php"><?= tr('Новини') ?></a>
        <span>&gt;</span>
        <span class="current"><?= $news_item['title'][$lang] ?></span>
    </div>
</section>

<section class="content">
    <div class="container">
        <div class="left-box">
            <img src="<?= $news_item['img'] ?>" alt="<?= $news_item['title'][$lang] ?>">
        </div>
        <div class="right-box">
            <div class="news-title"><?= $news_item['title'][$lang] ?></div>
            <div class="news-date"><?= $news_item['date'] ?></div>
            <div class="news-text"><?= $news_item['text'][$lang] ?></div>
            <div class="share">
                <span class="share-label"><?= tr('поділитися') ?></span>
                <ul class="share-menu">
                    <li>
                        <a onclick="window.open('http://www.facebook.com/sharer.php?p[url]=<?= urlencode( $url ); ?>&p[title]=<?= urlencode($news_item['title'][$lang]) ?>','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;"
                           class="facebook" title="Facebook"><i class="ikon-facebook"></i></a>
                    </li>
                    <li>
                        <a onclick="window.open('http://twitter.com/share?url=<?= urlencode($url) ?>&amp;text=<?= urlencode($news_item['title'][$lang]) ?>','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;"
                           class="twitter" title="Twitter"><i class="ikon-twitter"></i></a>
                    </li>
                    <li>
                        <a href="mailto:?subject=<?= urlencode($news_item['title'][$lang]) ?>&amp;body=<?= urlencode($url) ?>"><i class="ikon-mail"></i></a>
                    </li>
                    <li>
                        <a data-url="<?= $url ?>" class="copy-link" title="copy link" onclick="copyToClipboard(this.dataset.url)"><i class="ikon-copy-link"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<?php require_once 'php/footer.php' ?>

</body>
</html>
