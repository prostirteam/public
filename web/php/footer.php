<?php

?>
<footer>
    <div class="container">
        <div class="left-part">
            <h3><?= tr('Дізнавайтесь новини про ПРОСТІР') ?></h3>
            <form action="#">
                <input type="email" name="email" placeholder="<?= tr('Введіть пошту') ?>">
                <button><?= tr('Далі') ?></button>
            </form>
            <div class="socials">
                <a href="https://facebook.com" target="_blank"><i class="icon-fb"></i></a>
                <a href="https://youtube.com" target="_blank"><i class="icon-yt"></i></a>
            </div>
            <a href="#" class="sitemap"><?= tr('Мапа сайту') ?></a>
        </div>
        <div class="right-part">
            <h3><?= tr('НАШІ КОНТАКТИ') ?></h3>
            <div class="contact-item">
                <i class="icon-bank"></i>
                <span><?= tr('Національний банк України') ?></span>
            </div>
            <div class="contact-item">
                <i class="icon-location"></i>
                <span><?= tr('01601, Київ, вул. Інститутська, 9') ?></span>
            </div>
            <div class="contact-item">
                <i class="icon-envelope"></i>
                <span><?= tr('Контакти для зворотного зв’язку') ?></span>
            </div>
        </div>

        <div class="itkron-box"><?= tr('Розроблено компанією') ?> <a href="http://itkron.com">ITKron</a></div>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/js/lightslider.min.js"></script>
<script src="/js/jquery.matchHeight-min.js"></script>
<script src="/js/main.js?v=<?= filemtime('./js/main.js') ?>"></script>
<!--<script src="/js/pixel_p.js"></script>-->

