<?php
$banners = [
    [
        'img' => '/img/main-banner/slide_2.jpg',
        'text' => [
            'uk' => 'ПРОСТІР – це передові технології, що працюють для зручності людей та економічної безпеки держави',
            'ru' => 'ПРОСТІР – це передові технології, що працюють для зручності людей та економічної безпеки держави',
            'en' => 'ПРОСТІР – це передові технології, що працюють для зручності людей та економічної безпеки держави',
        ]
    ],
    [
        'img' => '/img/main-banner/slide_3.jpg',
        'text' => [
            'uk' => 'Програма Бізнес - Простір держателям карток для бізнесу',
            'ru' => 'Програма Бізнес - Простір держателям карток для бізнесу',
            'en' => 'Програма Бізнес - Простір держателям карток для бізнесу',
        ]
    ],
    [
        'img' => '/img/main-banner/nature.jpg',
        'text' => [
            'uk' => 'ПРОСТІР – це передові технології, що працюють для зручності людей та економічної безпеки держави',
            'ru' => 'ПРОСТІР – це передові технології, що працюють для зручності людей та економічної безпеки держави',
            'en' => 'ПРОСТІР – це передові технології, що працюють для зручності людей та економічної безпеки держави',
        ]
    ],
    [
        'img' => '/img/main-banner/slide_1.jpg',
        'text' => [
            'uk' => 'Програма Бізнес - Простір держателям карток для бізнесу',
            'ru' => 'Програма Бізнес - Простір держателям карток для бізнесу',
            'en' => 'Програма Бізнес - Простір держателям карток для бізнесу',
        ]
    ],
];

$news = [
    [
        'id' => 1,
        'img' => '/img/news/1.jpg',
        'date' => '01.11.18',
        'title' => [
            'uk' => 'АКЦІЯ ДЛЯ КІНОМАНІВ! Дивись українське разом з ПРОСТІР “Дике поле”',
            'ru' => 'АКЦІЯ ДЛЯ КІНОМАНІВ! Дивись українське разом з ПРОСТІР “Дике поле”',
            'en' => 'АКЦІЯ ДЛЯ КІНОМАНІВ! Дивись українське разом з ПРОСТІР “Дике поле”',
        ],
        'text' => [
            'uk' => '
                Розраховуйся карткою НПС “ПРОСТІР” та отримай безкоштовно квиток на показ українського фільму “Дике полеˮ.
                <br> <br>
                Учасником акції стати легко ‒ достатньо розрахуватися карткою НПС “ПРОСТІР” у будь-якій торговельній мережі або мережі Інтернет.
                <br> <br>
                Переможець обирається банком методом випадкової вибірки – виграє той, кому пощастить!
                <br> <br>
                <b>Акція діятиме з 01 до 31 жовтня 2018 року (включно).</b>
                <br> <br>
                У акції беруть участь держателі карток, емітованих такими банками:
                АТ “Ощадбанк”, АТ “Райффайзен Банк Аваль”, Полікомбанк, ПАТ “КБ “ГЛОБУС”,
                АТ “СКАЙ БАНК”, ПАТ “РВС БАНК”.
            ',
            'ru' => '
                Розраховуйся карткою НПС “ПРОСТІР” та отримай безкоштовно квиток на показ українського фільму “Дике полеˮ.
                <br> <br>
                Учасником акції стати легко ‒ достатньо розрахуватися карткою НПС “ПРОСТІР” у будь-якій торговельній мережі або мережі Інтернет.
                <br> <br>
                Переможець обирається банком методом випадкової вибірки – виграє той, кому пощастить!
                <br> <br>
                <b>Акція діятиме з 01 до 31 жовтня 2018 року (включно).</b>
                <br> <br>
                У акції беруть участь держателі карток, емітованих такими банками:
                АТ “Ощадбанк”, АТ “Райффайзен Банк Аваль”, Полікомбанк, ПАТ “КБ “ГЛОБУС”,
                АТ “СКАЙ БАНК”, ПАТ “РВС БАНК”.
            ',
            'en' => '
                Розраховуйся карткою НПС “ПРОСТІР” та отримай безкоштовно квиток на показ українського фільму “Дике полеˮ.
                <br> <br>
                Учасником акції стати легко ‒ достатньо розрахуватися карткою НПС “ПРОСТІР” у будь-якій торговельній мережі або мережі Інтернет.
                <br> <br>
                Переможець обирається банком методом випадкової вибірки – виграє той, кому пощастить!
                <br> <br>
                <b>Акція діятиме з 01 до 31 жовтня 2018 року (включно).</b>
                <br> <br>
                У акції беруть участь держателі карток, емітованих такими банками:
                АТ “Ощадбанк”, АТ “Райффайзен Банк Аваль”, Полікомбанк, ПАТ “КБ “ГЛОБУС”,
                АТ “СКАЙ БАНК”, ПАТ “РВС БАНК”.
            ',
        ],
    ],
    [
        'id' => 2,
        'img' => '/img/news/2.png',
        'date' => '01.11.18',
        'title' => [
            'uk' => 'Публічне акціонерне товариство Укрпошта приєдналось до НПС «ПРОСТІР» у якості еквайра.',
            'ru' => 'Публічне акціонерне товариство Укрпошта приєдналось до НПС «ПРОСТІР» у якості еквайра.',
            'en' => 'Публічне акціонерне товариство Укрпошта приєдналось до НПС «ПРОСТІР» у якості еквайра.',
        ],
        'text' => [
            'uk' => 'Публічне акціонерне товариство Укрпошта приєдналось до НПС «ПРОСТІР» у якості еквайра.',
            'ru' => 'Публічне акціонерне товариство Укрпошта приєдналось до НПС «ПРОСТІР» у якості еквайра.',
            'en' => 'Публічне акціонерне товариство Укрпошта приєдналось до НПС «ПРОСТІР» у якості еквайра.',
        ],
    ],
    [
        'id' => 3,
        'img' => '/img/news/3.png',
        'date' => '01.11.18',
        'title' => [
            'uk' => 'Замовляйте картку ПРОСТІР он-лайн!',
            'ru' => 'Замовляйте картку ПРОСТІР он-лайн!',
            'en' => 'Замовляйте картку ПРОСТІР он-лайн!',
        ],
        'text' => [
            'uk' => 'Замовляйте картку ПРОСТІР он-лайн!',
            'ru' => 'Замовляйте картку ПРОСТІР он-лайн!',
            'en' => 'Замовляйте картку ПРОСТІР он-лайн!',
        ],
    ],
    [
        'id' => 4,
        'img' => '/img/news/4.png',
        'date' => '01.11.18',
        'title' => [
            'uk' => 'Публічне акціонерне товариство Укрпошта приєдналось до НПС «ПРОСТІР» у якості еквайра.',
            'ru' => 'Публічне акціонерне товариство Укрпошта приєдналось до НПС «ПРОСТІР» у якості еквайра.',
            'en' => 'Публічне акціонерне товариство Укрпошта приєдналось до НПС «ПРОСТІР» у якості еквайра.',
        ],
        'text' => [
            'uk' => 'Публічне акціонерне товариство Укрпошта приєдналось до НПС «ПРОСТІР» у якості еквайра.',
            'ru' => 'Публічне акціонерне товариство Укрпошта приєдналось до НПС «ПРОСТІР» у якості еквайра.',
            'en' => 'Публічне акціонерне товариство Укрпошта приєдналось до НПС «ПРОСТІР» у якості еквайра.',
        ],
    ],
    [
        'id' => 5,
        'img' => '/img/news/5.png',
        'date' => '01.11.18',
        'title' => [
            'uk' => 'Картка Простір від банку центр он-лайн!',
            'ru' => 'Картка Простір від банку центр он-лайн!',
            'en' => 'Картка Простір від банку центр он-лайн!',
        ],
        'text' => [
            'uk' => 'Картка Простір від банку центр он-лайн!',
            'ru' => 'Картка Простір від банку центр он-лайн!',
            'en' => 'Картка Простір від банку центр он-лайн!',
        ],
    ],
    [
        'id' => 6,
        'img' => '/img/news/6.png',
        'date' => '01.11.18',
        'title' => [
            'uk' => 'Альфа-банк Україна розпочинає випуск пратіжних карток Простір',
            'ru' => 'Альфа-банк Україна розпочинає випуск пратіжних карток Простір',
            'en' => 'Альфа-банк Україна розпочинає випуск пратіжних карток Простір',
        ],
        'text' => [
            'uk' => 'Альфа-банк Україна розпочинає випуск пратіжних карток Простір',
            'ru' => 'Альфа-банк Україна розпочинає випуск пратіжних карток Простір',
            'en' => 'Альфа-банк Україна розпочинає випуск пратіжних карток Простір',
        ],
    ],
    [
        'id' => 7,
        'img' => '/img/news/7.png',
        'date' => '01.11.18',
        'title' => [
            'uk' => 'НПС "Простір" разом з партнерами Ощадбанк та @Пластиковакарта провели тиждень грошей #globalMoneyWeek',
            'ru' => 'НПС "Простір" разом з партнерами Ощадбанк та @Пластиковакарта провели тиждень грошей #globalMoneyWeek',
            'en' => 'НПС "Простір" разом з партнерами Ощадбанк та @Пластиковакарта провели тиждень грошей #globalMoneyWeek',
        ],
        'text' => [
            'uk' => 'НПС "Простір" разом з партнерами Ощадбанк та @Пластиковакарта взяли участь у проведенні всесвітнього тижня грошей #globalMoneyWeek',
            'ru' => 'НПС "Простір" разом з партнерами Ощадбанк та @Пластиковакарта взяли участь у проведенні всесвітнього тижня грошей #globalMoneyWeek',
            'en' => 'НПС "Простір" разом з партнерами Ощадбанк та @Пластиковакарта взяли участь у проведенні всесвітнього тижня грошей #globalMoneyWeek',
        ],
    ],
    [
        'id' => 8,
        'img' => '/img/news/8.png',
        'date' => '01.11.18',
        'title' => [
            'uk' => 'Акція для кіноманів! Дивись українське разом з Простір "Скажене вессіля"',
            'ru' => 'Акція для кіноманів! Дивись українське разом з Простір "Скажене вессіля"',
            'en' => 'Акція для кіноманів! Дивись українське разом з Простір "Скажене вессіля"',
        ],
        'text' => [
            'uk' => 'Акція для кіноманів! Дивись українське разом з Простір "Скажене вессіля"',
            'ru' => 'Акція для кіноманів! Дивись українське разом з Простір "Скажене вессіля"',
            'en' => 'Акція для кіноманів! Дивись українське разом з Простір "Скажене вессіля"',
        ],
    ],
];

$members_1 = [
    ['name' => 'Приват банк   ', 'img' => '/img/banks/privatbank.png',     'color' => '#5bb131', 'url' => 'https://privatbank.ua/ru' ],
    ['name' => 'Ощадбанк      ', 'img' => '/img/banks/oschadbank.png',     'color' => '#008247', 'url' => 'https://www.oschadbank.ua/ua/' ],
    ['name' => 'Ukrexim bank  ', 'img' => '/img/banks/ukreximbank.png',    'color' => '#112d6a', 'url' => 'https://www.eximb.com/ukr/personal/' ],
    ['name' => 'Райфайзен банк', 'img' => '/img/banks/raiffaisenbank.png', 'color' => '#feef00', 'url' => 'https://www.aval.ua/' ],
    ['name' => 'Правекс банк  ', 'img' => '/img/banks/praveksbank.png',    'color' => '#1b4c16', 'url' => 'https://www.pravex.com.ua/' ],
    ['name' => 'Сбербанк      ', 'img' => '/img/banks/sberbank.png',       'color' => '#007039', 'url' => 'https://www.sberbank.ua/' ],
    ['name' => 'Укргазбанк    ', 'img' => '/img/banks/ukrgazbank.png',     'color' => '#005395', 'url' => 'https://www.ukrgasbank.com/' ],
    ['name' => 'Skybank       ', 'img' => '/img/banks/skybank.png',        'color' => '#28a7e0', 'url' => 'https://www.sky.bank/' ],
    ['name' => 'Полікомбанк   ', 'img' => '/img/banks/polikombank.png',    'color' => '#3c60a7', 'url' => 'http://www.poli.com.ua/' ],
    ['name' => 'Банк Південний', 'img' => '/img/banks/pivdenyibank.png',   'color' => '#017055', 'url' => 'https://bank.com.ua/ru/' ],
    ['name' => 'Восток банк   ', 'img' => '/img/banks/vostokbank.png',     'color' => '#29383b', 'url' => 'https://bankvostok.com.ua/' ],
    ['name' => 'Глобус банк   ', 'img' => '/img/banks/globusbank.png',     'color' => '#60c6ee', 'url' => 'https://globusbank.com.ua/ru.html' ],
    ['name' => 'Банк Січ      ', 'img' => '/img/banks/banksich.png',       'color' => '#7c440f', 'url' => 'https://www.sichbank.com.ua/ru/' ],
    ['name' => 'Alpari Bank   ', 'img' => '/img/banks/alparibank.png',     'color' => '#20b696', 'url' => 'http://bankalpari.com/' ],
    ['name' => 'Альфа банк    ', 'img' => '/img/banks/Alfabank.png',       'color' => '#f22e13', 'url' => 'https://alfabank.ua/' ],
];
$members_2 = [
    ['name' => 'Otp bank      ', 'img' => '/img/banks/otpbank.png',        'color' => '#8dc22e', 'url' => 'https://ru.otpbank.com.ua/' ],
    ['name' => 'Таскомбанк    ', 'img' => '/img/banks/taskombank.png',     'color' => '#042f7e', 'url' => 'https://tascombank.ua/' ],
    [], [], [],
    ['name' => 'Rws bank      ', 'img' => '/img/banks/rwsbank.png',        'color' => '#a12136', 'url' => 'http://rwsbank.com.ua/' ],
    ['name' => 'Unex Bank     ', 'img' => '/img/banks/unexbank.png',       'color' => '#fe0000', 'url' => 'https://unexbank.ua/site/index.php' ],
    ['name' => 'Bank alliance ', 'img' => '/img/banks/bankalliance.png',   'color' => '#ffae1d', 'url' => 'https://bankalliance.ua/' ],
    ['name' => 'Center bank   ', 'img' => '/img/banks/center-bank.png',    'color' => '#009c92', 'url' => 'https://www.bankcenter.com.ua/' ],
    [],
    ['name' => 'Vernum bank   ', 'img' => '/img/banks/vernumbank.png',     'color' => '#065181', 'url' => 'https://vernumbank.com/ru.html' ],
    ['name' => 'Megabank      ', 'img' => '/img/banks/megabank.png',       'color' => '#e82825', 'url' => 'https://www.megabank.ua/' ],
    ['name' => 'ПУМБ          ', 'img' => '/img/banks/pumb.png',           'color' => '#d13239', 'url' => 'https://www.pumb.ua/ru' ],
    ['name' => 'MTB bank      ', 'img' => '/img/banks/mtbbank.png',        'color' => '#2baf32', 'url' => 'https://mtb.ua/' ],
    ['name' => 'Рада банк     ', 'img' => '/img/banks/radabank.png',       'color' => '#271b72', 'url' => 'https://www.radabank.com.ua/' ],
];

$members_3 = [
    ['name' => 'Center bank                  ', 'img' => '/img/banks/centerbank.png',                      'color' => '#009c92', 'url' => 'https://www.bankcenter.com.ua/' ],
    ['name' => 'Укрсоцбанк                   ', 'img' => '/img/banks/ukrsocbank.png',                      'color' => '#c4303b', 'url' => 'https://ru.ukrsotsbank.com/' ],
    ['name' => 'Creditwest bank              ', 'img' => '/img/banks/creditwest_bank.png',                 'color' => '#002aa1', 'url' => 'https://www.creditwest.ua/uk/' ],
    ['name' => 'Credit europe bank           ', 'img' => '/img/banks/credit_europe_bank.png',              'color' => '#ff001e', 'url' => 'http://www.crediteurope.com.ua/ru/' ],
    ['name' => 'OKCI bank                    ', 'img' => '/img/banks/oksibank.png',                        'color' => '#76d8ef', 'url' => 'https://www.okcibank.com.ua/' ],
    ['name' => 'МЕТА банк                    ', 'img' => '/img/banks/metabank.png',                        'color' => '#005196', 'url' => 'http://www.mbank.com.ua/' ],
    ['name' => 'Kredo bank                   ', 'img' => '/img/banks/kredobank.png',                       'color' => '#00479f', 'url' => 'https://www.kredobank.com.ua/' ],
    ['name' => 'Банк інвестицій та заощаджень', 'img' => '/img/banks/bank_investyciy_ta_zaoschadzhen.png', 'color' => '#ffed00', 'url' => 'https://www.bisbank.com.ua/' ],
    ['name' => 'Банк Кредит Дніпро           ', 'img' => '/img/banks/bank_kredit_dnipro.png',              'color' => '#00aa48', 'url' => 'https://creditdnepr.com.ua/ru' ],
    ['name' => 'IBOX BANK                    ', 'img' => '/img/banks/ibox_bank.png',                       'color' => '#a51f9b', 'url' => 'http://www.iboxbank.online/' ],
    ['name' => 'Европромбанк                 ', 'img' => '/img/banks/evroprombank.png',                    'color' => '#e3182c', 'url' => 'https://europrombank.kiev.ua/' ],
    ['name' => 'International investment bank', 'img' => '/img/banks/international_investment_bank.png',   'color' => '#00a977', 'url' => 'https://iib.int/en' ],
    ['name' => 'Комінвест банк               ', 'img' => '/img/banks/cominvest_bank.png',                  'color' => '#416197', 'url' => 'http://www.coopinvest.com.ua/uk' ],
    ['name' => 'Concord bank                 ', 'img' => '/img/banks/concordbank.png',                     'color' => '#69c925', 'url' => 'https://concord.ua/ru' ],
    ['name' => 'Банк Львів                   ', 'img' => '/img/banks/banklviv.png',                        'color' => '#c81550', 'url' => 'https://www.banklviv.com/' ],
    ['name' => 'УБІБ                         ', 'img' => '/img/banks/ubib.png',                            'color' => '#ffad1f', 'url' => 'https://ubib.com.ua/' ],
    ['name' => 'Банк Фамільний               ', 'img' => '/img/banks/bank_familniy.png',                   'color' => '#521100', 'url' => 'http://www.fbank.com.ua/' ],
    ['name' => 'Com-in-bank                  ', 'img' => '/img/banks/cominbank.png',                       'color' => '#007d89', 'url' => 'https://cib.com.ua/ru/' ],
    ['name' => 'PINbank                      ', 'img' => '/img/banks/pinbank.png',                         'color' => '#601f87', 'url' => 'https://www.pinbank.ua/' ],
    ['name' => 'ГРАНТ                        ', 'img' => '/img/banks/grant.png',                           'color' => '#0d3d81', 'url' => 'https://www.grant.ua/' ],
    ['name' => 'КРИСТАЛБАНК                  ', 'img' => '/img/banks/crystalbank.png',                     'color' => '#00c8f7', 'url' => 'https://crystalbank.com.ua/' ],
    ['name' => 'Індустріалбанк               ', 'img' => '/img/banks/industrial_bank.png',                 'color' => '#005faf', 'url' => 'https://industrialbank.ua/ua/' ],
    ['name' => 'altbank                      ', 'img' => '/img/banks/altbank.png',                         'color' => '#001143', 'url' => 'https://altbank.ua/' ],
    ['name' => 'Укрпошта                     ', 'img' => '/img/banks/ukrposhta.png',                       'color' => '#ffbf00', 'url' => 'https://ukrposhta.ua/ru/' ],
    ['name' => 'Траст Капитал                ', 'img' => '/img/banks/trust_kapital.png',                   'color' => '#dd9a00', 'url' => 'http://www.tc-bank.com/ru.html' ],
];

$all_members = array_filter(array_merge($members_1, $members_2, $members_3), function ($item) {return $item;});

$services = [
    [
        'img' => '/img/services_icons/1.svg',
        'title' => [
            'uk' => 'Віртуальні картки',
            'ru' => 'Виртуальные карты',
            'en' => 'Virtual Cards',
        ],
        'text' => [
            'uk' => 'Для тих, хто часто здійснює покупки в українських онлайн-магазинах, стане в нагоді віртуальна платіжна картка ПРОСТІР. Віртуальна картка мінімізує ризики шахрайства при розрахунках в Інтернет',
            'ru' => 'Для тих, хто часто здійснює покупки в українських онлайн-магазинах, стане в нагоді віртуальна платіжна картка ПРОСТІР. Віртуальна картка мінімізує ризики шахрайства при розрахунках в Інтернет',
            'en' => 'Virtual PROSTIR card will be useful for active clients of Ukrainian online shops. Virtual Card ensures minimum risk of fraud in Internet payments',
        ]
    ],
    [
        'img' => '/img/services_icons/2.svg',
        'title' => [
            'uk' => 'Безконтактні платежі',
            'ru' => 'Безконтактные платежи',
            'en' => 'Contactless payments',
        ],
        'text' => [
            'uk' => 'Картки ПРОСТІР мають безконтактну технологію. Вже сьогодні картками ПРОСТІР можна оплатити проїзд у Київському метрополітені безпосередньо на турнікеті',
            'ru' => 'Картки ПРОСТІР мають безконтактну технологію. Вже сьогодні картками ПРОСТІР можна оплатити проїзд у Київському метрополітені безпосередньо на турнікеті',
            'en' => 'PROSTIR cards have a built-in contactless technology. PROSTIR cards allow one to pay for undeground trips in Kyiv directly on a ticket-gate',
        ]
    ],
    [
        'img' => '/img/services_icons/3.svg',
        'title' => [
            'uk' => 'Перекази з картки на картку',
            'ru' => 'Перевод с карты на карту',
            'en' => 'Card-to-Card Payments',
        ],
        'text' => [
            'uk' => 'Держатель картки ПРОСТІР зможе здійснити грошовий переказ зі своєї картки на картку іншого держателя картки ПРОСТІР. Дана послуга буде доступна найближчим часом',
            'ru' => 'Держатель картки ПРОСТІР зможе здійснити грошовий переказ зі своєї картки на картку іншого держателя картки ПРОСТІР. Дана послуга буде доступна найближчим часом',
            'en' => 'PROSTIR card holder will be able to pay from his/her card to another PROSTIR card The service will be available shortly',
        ]
    ],
    [
        'img' => '/img/services_icons/4.svg',
        'title' => [
            'uk' => 'Бонусні програми',
            'ru' => 'Бонусные програмы',
            'en' => 'Bonus Programs',
        ],
        'text' => [
            'uk' => 'Платіжна система ПРОСТІР прагне винагороджувати держателів карток ПРОСТІР за користування ними. Тому, незабаром, ПРОСТІР запропонує власну програму лояльності',
            'ru' => 'Платіжна система ПРОСТІР прагне винагороджувати держателів карток ПРОСТІР за користування ними. Тому, незабаром, ПРОСТІР запропонує власну програму лояльності',
            'en' => 'PROSTIR payment system strives to reward PROSTIR card holders for loyalty. Therefore, PROSTIR will soon introduce its own loyalty program',
        ]
    ],
    [
        'img' => '/img/services_icons/5.svg',
        'title' => [
            'uk' => 'Електронна комерція',
            'ru' => 'Электронная коммерция',
            'en' => 'E-commerce',
        ],
        'text' => [
            'uk' => 'Картками ПРОСТІР зручно та безпечно розраховуватися в українському Інтернет просторі',
            'ru' => 'Картками ПРОСТІР зручно та безпечно розраховуватися в українському Інтернет просторі',
            'en' => 'PROSTIR cards are an easy and secure way to payin the Ukrainian Internet domain',
        ]
    ],
    [
        'img' => '/img/services_icons/6.svg',
        'title' => [
            'uk' => 'Мобільний додаток ПРОСТІР',
            'ru' => 'Мобильное приложение ПРОСТИР',
            'en' => 'PROSTIR Mobile App',
        ],
        'text' => [
            'uk' => 'Держателі карток ПРОСТІР зможуть швидко та зручно користуватися сервісами платіжної системи ПРОСТІР за допомогою мобільного додатку. Мобільний додаток ПРОСТІР буде доступний найближчим часом',
            'ru' => 'Держателі карток ПРОСТІР зможуть швидко та зручно користуватися сервісами платіжної системи ПРОСТІР за допомогою мобільного додатку. Мобільний додаток ПРОСТІР буде доступний найближчим часом',
            'en' => 'PROSTIR Card holders will get an easy access PROSTIR payment system services through a mobile app. PROSTIR mobile app will be available shortly',
        ]
    ],
];

$banks_in_consumers_page = [
    ['name' => 'Ощадбанк      ', 'img' => '/img/banks/oschadbank.png',     'color' => '#008247', 'url' => 'https://www.oschadbank.ua/ua/' ],
    ['name' => 'Райфайзен банк', 'img' => '/img/banks/raiffaisenbank.png', 'color' => '#feef00', 'url' => 'https://www.aval.ua/' ],
    ['name' => 'Укргазбанк    ', 'img' => '/img/banks/ukrgazbank.png',     'color' => '#005395', 'url' => 'https://www.ukrgasbank.com/' ],
    ['name' => 'Полікомбанк   ', 'img' => '/img/banks/polikombank.png',    'color' => '#3c60a7', 'url' => 'http://www.poli.com.ua/' ],
    ['name' => 'Альфа банк    ', 'img' => '/img/banks/Alfabank.png',       'color' => '#f22e13', 'url' => 'https://alfabank.ua/' ],
    ['name' => 'Сenter bank   ', 'img' => '/img/banks/center-bank.png',    'color' => '#2aafa8', 'url' => 'https://www.bankcenter.com.ua/' ],
    ['name' => 'Глобус банк   ', 'img' => '/img/banks/globusbank.png',     'color' => '#60c6ee', 'url' => 'https://globusbank.com.ua/ru.html' ],
    ['name' => 'Skybank       ', 'img' => '/img/banks/skybank.png',        'color' => '#28a7e0', 'url' => 'https://www.sky.bank/' ],
    ['name' => 'Rws bank      ', 'img' => '/img/banks/rwsbank.png',        'color' => '#a12136', 'url' => 'http://rwsbank.com.ua/' ],
    ['name' => 'Bank alliance ', 'img' => '/img/banks/bankalliance.png',   'color' => '#ffae1d', 'url' => 'https://bankalliance.ua/' ],
];
