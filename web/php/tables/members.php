<table>
    <thead>
    <tr>
        <th>№ з/n</th>
        <th>Назва учасника</th>
        <th>Свідоцтво про членство та надання ліцензії на використання торговельної марки</th>
        <th>Функції</th>
        <th>Примітка</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td></td>
        <td>Національний банк України</td>
        <td>-</td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Власник НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>1</td>
        <td><a target="_blank" href="http://www.megabank.net/">ПАТ "Мегабанк"</a></td>
        <td>
            <div>146 від 24.01.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td><a target="_blank" href="http://www.grant.kharkov.ua/">ПАТ "Банк "Грант"</a></td>
        <td>
            <div>2 від 15.06.2008</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>3</td>
        <td><a target="_blank" href="http://www.policombank.com/">"Полікомбанк"</a></td>
        <td>
            <div>136 від 16.11.2016</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td><a target="_blank" href="http://www.pivdenny.com/">Акціонерний банк "Південний"</a></td>
        <td>
            <div>144 від 26.12.2016</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td><a target="_blank" href="http://www.cib.com.ua/">АТ "КІБ"</a></td>
        <td>
            <div>157 від 20.04.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>6</td>
        <td><a target="_blank" href="http://www.fib.com.ua/">ПАТ "ПершийІнвестиційний Банк"</a></td>
        <td>
            <div>175 від 25.06.2018</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>7</td>
        <td><a target="_blank" href="http://www.banklviv.com/">ПАТ АКБ "Львів"</a></td>
        <td>
            <div>138 від 18.11.2016</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>8</td>
        <td><a target="_blank" href="http://www.unexbank.kiev.ua/">ПАТ "Юнекс банк"</a></td>
        <td>
            <div>161 від 30.05.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>9</td>
        <td><a target="_blank" href="http://www.ukrgasbank.com/">АБ "Укргазбанк"</a></td>
        <td>
            <div>132 від 07.10.2016</div>
        </td>
        <td>
            <div>емітент/еквайр МПІ</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>10</td>
        <td><a target="_blank" href="http://www.creditdnepr.com.ua/">ПАТ "Банк Кредит Дніпро"</a></td>
        <td>
            <div>147 від 24.01.2017</div>
        </td>
        <td>
            <div>емітент/еквайр МПІ</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>11</td>
        <td><a target="_blank" href="http://www.creditwest.kiev.ua/">ПАТ "КРЕДИТВЕСТ БАНК"</a></td>
        <td>
            <div>54 від 02.07.2007</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>12</td>
        <td><a target="_blank" href="http://www.crediteurope.com.ua/">ПАТ "КРЕДИТ ЄВРОПА БАНК"</a></td>
        <td>
            <div>169 від 29.12.2017</div>
        </td>
        <td>емітент</td>
        <td></td>
    </tr>
    <tr>
        <td>13</td>
        <td><a target="_blank" href="http://www.okcibank.com.ua/">ПАТ "ОКСІ БАНК"</a></td>
        <td>
            <div>56 від 19.05.2009</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>14</td>
        <td><a target="_blank" href="http://www.mbank.com.ua/">АТ "МетаБанк"</a></td>
        <td>
            <div>165 від 11.07.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>15</td>
        <td><a target="_blank" href="http://www.kredobank.com.ua/">ПАТ "КРЕДОБАНК"</a></td>
        <td>
            <div>172 від 27.03.2018</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>16</td>
        <td><a target="_blank" href="http://www.bisbank.com.ua/">ПАТ "Банк інвестицій тазаощаджень"</a></td>
        <td>
            <div>142 від 13.12.2016</div>
        </td>
        <td>еквайр</td>
        <td></td>
    </tr>
    <tr>
        <td>17</td>
        <td><a target="_blank" href="http://www.eximb.com/">АТ "Укрексімбанк"</a></td>
        <td>
            <div>149 від 21.02.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>18</td>
        <td><a target="_blank" href="http://www.oschadnybank.com/">АТ "Ощадбанк"</a></td>
        <td>
            <div>159 від 04.05.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>19</td>
        <td><a target="_blank" href="http://www.iboxbank.online/">ПАТ "Айбокс Банк"</a></td>
        <td>
            <div>73 від 12.10.2011</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>20</td>
        <td><a target="_blank" href="http://www.radabank.com.ua/">ПАТ "РАДАБАНК"</a></td>
        <td>
            <div>168 від 23.11.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>21</td>
        <td><a target="_blank" href="http://europrombank.com/">ПАТ "ЄВРОПРОМБАНК"</a></td>
        <td>
            <div>81 від 21.02.2013</div>
        </td>
        <td>емітент</td>
        <td></td>
    </tr>
    <tr>
        <td>22</td>
        <td><a target="_blank" href="http://www.ii-bank.com.ua/">ПАТ "МІБ"</a></td>
        <td>
            <div>150 від 27.02.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>23</td>
        <td><a target="_blank" href="http://www.atcominvestbank.com/">АТ "Комінвестбанк"</a></td>
        <td>
            <div>182 від 01.11.2018</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>24</td>
        <td><a target="_blank" href="http://privatbank.ua/">АТ КБ "ПРИВАТБАНК"</a></td>
        <td>
            <div>170 від 11.01.2018</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>25</td>
        <td><a target="_blank" href="http://www.otpbank.com.ua/">ПАТ "ОТП Банк"</a></td>
        <td>
            <div>152 від 02.03.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>26</td>
        <td><a target="_blank" href="http://www.aval.ua/">АТ "Райффайзен банк Аваль"</a></td>
        <td>
            <div>134 від 17.10.2016</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>27</td>
        <td><a target="_blank" href="http://www.tascombank.com.ua/">ПАТ "Таскомбанк"</a></td>
        <td>
            <div>148 від 02.02.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>28</td>
        <td><a target="_blank" href="http://www.vernumbank.com/">ПАТ "Вернум банк"</a></td>
        <td>
            <div>164 від 27.06.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>29</td>
        <td><a target="_blank" href="http://www.region-bank.com.ua/">ПАТ "СКАЙ БАНК"</a></td>
        <td>
            <div>163 від 20.06.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>30</td>
        <td><a target="_blank" href="http://www.concord.ua/">ПАТ "АКБ "КОНКОРД"</a></td>
        <td>
            <div>158 від 26.04.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>31</td>
        <td><a target="_blank" href="http://bankvostok.com.ua/">ПАТ "Банк Восток"</a></td>
        <td>
            <div>130 від 26.09.2016</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>32</td>
        <td><a target="_blank" href="https://www.sberbank.ua/">ПАТ "СБЕРБАНК"</a></td>
        <td>
            <div>140 від 07.12.2016</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>33</td>
        <td><a target="_blank" href="http://www.globusbank.com.ua/">ПАТ "КБ "ГЛОБУС"</a></td>
        <td>
            <div>137 від 16.11.2016</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>34</td>
        <td><a target="_blank" href="https://www.unicredit.ua/">ПАТ "УКРСОЦБАНК"</a></td>
        <td>
            <div>151 від 27.02.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>35</td>
        <td>
            <div><a target="_blank" href="http://ubib.com.ua/">ПАТ "Український Будівельно –Інвестиційний Банк"</a>
            </div>
        </td>
        <td>
            <div>141 від 07.12.2016</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>36</td>
        <td><a target="_blank" href="http://pumb.ua/ua">ПАТ "Перший українськийміжнародний банк"</a></td>
        <td>
            <div>162 від 20.06.2017</div>
        </td>
        <td>
            <div>еквайр/процесинговий центр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>37</td>
        <td><a target="_blank" href="http://www.pravex.com/">АТ "ПРАВЕКС БАНК"</a></td>
        <td>
            <div>173 від 14.05.2018</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>38</td>
        <td><a target="_blank" href="http://bankalpari.com/">АТ "АЛЬПАРІ БАНК"</a></td>
        <td>
            <div>174 від 17.05.2018</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>39</td>
        <td><a target="_blank" href="http://www.sichbank.com.ua/">ПАТ "БАНК СІЧ"</a></td>
        <td>
            <div>131 від 05.10.2016</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>40</td>
        <td><a target="_blank" href="http://www.fbank.com.ua/">ПАТ "Фамільний"</a></td>
        <td>
            <div>133 від 07.10.2016</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>41</td>
        <td><a target="_blank" href="https://alfabank.ua/">ПАТ "АЛЬФА-БАНК"</a></td>
        <td>
            <div>143 від 22.12.2016</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>42</td>
        <td><a target="_blank" href="http://rwsbank.com.ua/">ПАТ "РВС БАНК"</a></td>
        <td>
            <div>153 від 12.04.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>43</td>
        <td><a target="_blank" href="http://alliancebank.org.ua/">ПАТ "БАНК АЛЬЯНС"</a></td>
        <td>
            <div>154 від 14.04.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>44</td>
        <td><a target="_blank" href="https://crystalbank.com.ua/">ПАТ "Кристалбанк"</a></td>
        <td>
            <div>166 від 02.08.2017</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>45</td>
        <td><a target="_blank" href="http://marfinbank.ua/web/mb-ua.nsf/0/mainpage">ПАТ "МТБ БАНК"</a></td>
        <td>
            <div>171 від 21.03.2018</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td>
            <div>Підключений до Центрального маршрутизатора НПС "ПРОСТІР"</div>
        </td>
    </tr>
    <tr>
        <td>46</td>
        <td><a target="_blank" href="https://ukrposhta.ua/pro-pidpriyemstvo/zagalna-informaciya/">ПАТ "Укрпошта"</a>
        </td>
        <td>
            <div>176 від 03.07.2018</div>
        </td>
        <td>еквайр</td>
        <td></td>
    </tr>
    <tr>
        <td>47</td>
        <td><a target="_blank" href="https://industrialbank.ua/ua/">АКБ "ІНДУСТРІАЛБАНК"</a></td>
        <td>
            <div>177 від 25.07.2018</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>48</td>
        <td><a target="_blank" href="https://altbank.ua/">АТ "АЛЬТБАНК"</a></td>
        <td>
            <div>178 від 16.08.2018</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>49</td>
        <td><a target="_blank" href="http://www.tc-bank.com/">ПАТ АКБ "Траст-капітал"</a></td>
        <td>
            <div>179 від 18.09.2018</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>50</td>
        <td><a target="_blank" href="https://bank-portal.com.ua/">ПАТ "БАНК ПОРТАЛ"</a></td>
        <td>
            <div>180 від 01.10.2018</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>51</td>
        <td><a target="_blank" href="http://www.poltavabank.com/home/">АТ "Полтава-банк"</a></td>
        <td>
            <div>181 від 01.11.2018</div>
        </td>
        <td>
            <div>емітент/еквайр</div>
        </td>
        <td></td>
    </tr>
    </tbody>
</table>
