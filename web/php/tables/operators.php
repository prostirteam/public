<table>
    <thead>
    <tr>
        <th>№<br>п/п</th>
        <th>Назва юридичної особи</th>
        <th>Юридична адреса</th>
        <th>Види послуг оператора</th>
        <th>Підстава<br>(протокол:<br>дата, №)</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td><a target="_blank" href="http://www.portmone.com/">ТОВ "ПОРТМОНЕ"</a></td>
        <td>
            <div><a target="_blank" href="mailto:support@portmone.com">вул. Північно-Сирецька, 1-3, м. Київ, 04136,
                    Україна.<br><br>Для листів: Україна, 04136, м. Київ – 136,<br>а/с 5.<br><br>Тел/факс:<br>+380 44 200
                    09 02<br>E-mail:<br>support@portmone.com</a></div>
        </td>
        <td>
            <div>1. Технологічне, інформаційне обслуговування отримувача переказу.<br>2. Технологічне,
                інформаційнеобслуговування еквайрів заопераціями переказу коштів.<br>3. Обробка даних під час формування
                платіжних доручень в електронному вигляді.
            </div>
        </td>
        <td>
            <div>протокол від 22.12.2016 №57/329/2016</div>
        </td>
    </tr>
    </tbody>
</table>
