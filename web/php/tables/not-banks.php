<table>
    <thead>
    <tr>
        <th>№ n/n</th>
        <th>Назва установи</th>
        <th>Місцезнаходження установи</th>
        <th>Функції установи</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td><a target="_blank" href="http://www.banksoft.com.ua/">ТОВ СНВФ "Аргус"</a></td>
        <td>
            <div>вул. Ромена Роллана, 12, м. Харків, 61058</div>
        </td>
        <td>
            <div>банківський процесинговий центр</div>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td><a target="_blank" href="http://www.ukrcard.com.ua/">АТ "УКРКАРТ"</a></td>
        <td>
            <div>вул. Ордаша, буд. 19, с. Безпечна, Сквирський район, Київська область, Україна, 09025</div>
        </td>
        <td>
            <div>незалежний процесинговий центр, інформаційний еквайр</div>
        </td>
    </tr>
    <tr>
        <td>3</td>
        <td><a target="_blank" href="http://www.upc.ua/">ПрАТ "Український процесінговий центр"</a></td>
        <td>
            <div>пр-кт Степана Бандери, 9, корпус 5А, п/с №65</div>
        </td>
        <td>
            <div>незалежний процесинговий центр</div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Київ, 04073</td>
        <td></td>
    </tr>
    <tr>
        <td>4</td>
        <td><a target="_blank" href="http://www.ufn.com.ua/">ПАТ "УКРАЇНСЬКА ФІНАНСОВА МЕРЕЖА"</a></td>
        <td>
            <div>вул. Північно-Сирецька, 1-3, м. Київ, 04136</div>
        </td>
        <td>
            <div>незалежний процесинговий центр</div>
        </td>
    </tr>
    <tr>
        <td>5</td>
        <td><a target="_blank" href="http://www.taslink.com.ua/tl_contacts_ua.html">ТОВ "ТАС Лінк"</a></td>
        <td>
            <div>пр. Перемоги, м. Київ, 6503062</div>
        </td>
        <td>
            <div>незалежний процесинговий центр</div>
        </td>
    </tr>
    <tr>
        <td>6</td>
        <td>ТОВ "ІЗІ СОФТ"</td>
        <td>
            <div>вул. Межигірська, 82А, корп. Б, оф.312-А, 04080, Київ</div>
        </td>
        <td>
            <div>незалежний процесинговий центр</div>
        </td>
    </tr>
    <tr>
        <td>7</td>
        <td><a target="_blank" href="http://www.sts.gov.ua/">Державна податкова служба України</a></td>
        <td>
            <div>Львівська площа,8, м. Київ-53, 04655</div>
        </td>
        <td>
            <div>учасник з особливим статусом</div>
        </td>
    </tr>
    <tr>
        <td>8</td>
        <td><a target="_blank" href="http://www.fg.gov.ua/">Фонд гарантування вкладів фізичних осіб</a></td>
        <td>
            <div>б-р Т.Шевченка 33-б, м. Київ, 01032</div>
        </td>
        <td>
            <div>учасник з особливим статусом</div>
        </td>
    </tr>
    <tr>
        <td>9</td>
        <td>ТОВ "Агенція технічна "ЛОГОС"</td>
        <td>
            <div>вул. Б. Хмельницького, б. 106, м. Львів, 79024,</div>
        </td>
        <td>
            <div>інформаційний еквайр</div>
        </td>
    </tr>
    <tr>
        <td>10</td>
        <td><a target="_blank" href="http://www.ics-tech.kiev.ua/index.php">ТОВ "ІКС-ТЕХНО"</a></td>
        <td>
            <div>вул. Маршала Гречко, 7, м. Київ, Україна, 04136</div>
        </td>
        <td>
            <div>інформаційний еквайр</div>
        </td>
    </tr>
    <tr>
        <td>11</td>
        <td><a target="_blank" href="http://www.gera-service.com.ua/">ТОВ "ГЕРА-СЕРВІС"</a></td>
        <td>
            <div>вул. Стеценко, 19, корп. 68, м. Київ, 04128</div>
        </td>
        <td>
            <div>інформаційний еквайр</div>
        </td>
    </tr>
    <tr>
        <td>12</td>
        <td><a target="_blank" href="http://firmradio.net/">ТОВ-Фірма "Радіо"</a></td>
        <td>
            <div>вул. Збишко, 7, м. Вінниця, 21016</div>
        </td>
        <td>
            <div>інформаційний еквайр</div>
        </td>
    </tr>
    <tr>
        <td>13</td>
        <td><a target="_blank" href="https://fiscal.systems/">ТОВ "ФІСКАЛЬНІ СИСТЕМИ"</a></td>
        <td>
            <div>вул. Зоологічна, б. 4-А, офіс 139, м. Київ, 04119</div>
        </td>
        <td>
            <div>інформаційний еквайр</div>
        </td>
    </tr>
    <tr>
        <td>14</td>
        <td>ТОВ "Система ТРЕЙД"</td>
        <td>
            <div><a target="_blank" href="mailto:info@systemgroup.com.ua">03124, Україна, Київ, вул. Вацлава Гавела,
                    4<br>info@systemgroup.com.ua</a></div>
        </td>
        <td>
            <div>інформаційний еквайр</div>
        </td>
    </tr>
    <tr>
        <td>15</td>
        <td>
            <div>Державне підприємство "Український державний науково – дослідний інститут технологій товарно –
                грошового обігу, фінансових і фондових ринків "УКРЕЛЕКОН"
            </div>
        </td>
        <td>
            <div>вул. Довженка, 3, м. Київ, 03680</div>
        </td>
        <td>
            <div>інформаційний еквайр</div>
        </td>
    </tr>
    </tbody>
</table>
