<?php
?>
<header>
    <div class="top-part">
        <div class="container">
            <a class="logo-box" href="/">
                <img src="/img/logo.svg" class="logo" alt="PROSTIR">
            </a>
            <form class="search-form">
                <input type="text">
                <i class="icon-search js-search-btn"></i>
            </form>
            <a href="/" class="header-link">
                <span class="text"><?= tr('Кабінет програми лояльності') ?></span>
                <i class="icon-cabinet"></i>
            </a>
            <a href="/" class="header-link">
                <span class="text"><?= tr('Особистий кабінет') ?></span>
                <i class="icon-user"></i>
            </a>
            <div class='menu-toggle js-menu-toggle'>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class='mobile-input-wrapper'>
            <input type="text" placeholder='Пошук...'>
            <div class='mobile-input-wrapper__close'>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <div class="bottom-part">
        <div class="container">
            <nav>
                <a href="/about.php"     class="nav-item"><?= tr('Про Нас') ?></a>
                <a href="/news.php"      class="nav-item"><?= tr('Новини') ?></a>
                <a href="/services.php"  class="nav-item"><?= tr('Сервіси') ?></a>
                <a href="/banks.php"     class="nav-item"><?= tr('Банкам') ?></a>
                <a href="/consumers.php" class="nav-item"><?= tr('Приватним особам') ?></a>
                <a href="/business.php"  class="nav-item"><?= tr('Бізнесу') ?></a>
                <a href="/members.php"   class="nav-item"><?= tr('Учасники') ?></a>
                <div class='mobile-links-wrapper'>
                    <div class="lang-box js-show-langs">
                        <div class="lang"><?= $lang ?></div>
                        <div class="arrow-icon"></div>
                        <div class="dropdown">
                            <a class="dropdown-item" onclick="document.cookie = 'lang=uk'; location.reload();">UK</a>
                            <a class="dropdown-item" onclick="document.cookie = 'lang=ru'; location.reload();">RU</a>
                            <a class="dropdown-item" onclick="document.cookie = 'lang=en'; location.reload();">EN</a>
                        </div>
                    </div>
                    <a href="/" class="header-link header-link__mobile">
                        <span class="text"><?= tr('Кабінет програми лояльності') ?></span>
                        <i class="icon-cabinet"></i>
                    </a>
                    <a href="/" class="header-link header-link__mobile">
                        <span class="text"><?= tr('Особистий кабінет') ?></span>
                        <i class="icon-user"></i>
                    </a>
                </div>
            </nav>
        </div>
    </div>
</header>
