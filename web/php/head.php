<head>
    <meta charset="UTF-8">
    <title>ПРОСТІР - національна платіжна система</title>
    <meta name="keywords" content="простір, український платіжний простір, нпс, національна система,&nbsp;EMV,&nbsp;нсмеп,
        національна картка, українська картка, платіжна система НБУ, розрахунки, система нацбанка, бюджетні виплати">
    <meta name="description" content="НПС «ПРОСТІР» - національна платіжна система України. Обирай картку ПРОСТІР
        як простий та надійний засіб здійснення будь-яких операцій.">
    <link rel="icon" type="image/png" href="/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/site.css?v=<?= filemtime('./css/site.css') ?>">
</head>
