/**___________________________________________________________________________________________________________________*/
!function (window, $) {
    $(function () {
        var $gg = $('<div id="gg"></div>').appendTo('body');

        $(`<style> #gg{ width: 100%; position: fixed; border-top: 1px solid #000; height: 10000px; left: 0;
         z-index: 1000; top: 0; display: block; opacity: .5;
         background: url(gg.png) 0 0.2vw / 100% no-repeat;} </style>`).appendTo('body');

        localStorage.ggDisplay === 'block' ? $gg.show() : $gg.hide();
        if(localStorage.ggPos) $('#gg').css('background-position-y', localStorage.ggPos);
        $('.sliderBox').width($('.sliderItem').length * 33 + 'vw');
        if($('.projectsPage').length) {
            $('.projectsPage .item p').each(function () {
                $(this).attr('data-height', $(this).height() / $(window).width());
                if($(this).height() / $(window).width() > 0.1) { $(this).addClass('hidePart'); }
            });
        }

        $(document).on('keydown', function (e) {
            var pos = parseInt($gg.css('background-position-y'));
            if(e.which === 219) { $gg.css('background-position-y', pos + 10 + 'px'); }
            if(e.which === 221) { $gg.css('background-position-y', pos - 10 + 'px'); }
            if(e.which === 222) {
                if(localStorage.ggDisplay === 'none') {
                    $gg.show();
                    localStorage.ggDisplay = 'block';
                } else {
                    $gg.hide();
                    localStorage.ggDisplay = 'none';
                }
            }
            localStorage.ggPos = $gg.css('background-position-y');
        });
    });
}(window, $);
/**___________________________________________________________________________________________________________________*/