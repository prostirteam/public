var $b = $(document.body);
$('.new-item-title').matchHeight();
$('.services-item-text').matchHeight();
$('.advantages-item-text').matchHeight();

$('.js-menu-toggle').click(function(){
    $(this).toggleClass('-active');
    $('.bottom-part').slideToggle('slow');
});

$('.js-search-btn').click(function(){
    $('.mobile-input-wrapper').toggleClass('-active');
});

$('.mobile-input-wrapper__close').click(function(){
    $('.mobile-input-wrapper').removeClass('-active');
});


$b.on('click', function(e) {
    if($(e.target).closest('.js-show-langs').length) return;
    $('.dropdown').slideUp(100);
});

$b.on('click', '.js-show-langs', function() {
    $(this).find('.dropdown').slideToggle(100);
});

$('body.index-page .news-slider').lightSlider({
    item: 3,
    loop: true,
    slideMove: 2,
    easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    speed: 1000,
    auto: false,
    responsive : [{breakpoint: 1200, settings: {item: 2}}, {breakpoint: 575, settings: {item: 1}}]
});

$('body.index-page .partners-slider').lightSlider({
    item: 4,
    loop: true,
    slideMove: 2,
    easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    speed: 3000,
    auto: true,
    responsive : [{breakpoint: 1200, settings: {item: 2}}]
});

$('body.index-page #banner-section .slide-box').lightSlider({
    item: 1,
    loop: true,
    easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    speed: 2000,
    pause: 4000,
    auto: true,
    mode: "fade",
    slideMargin: 0,
});

$('body.members-page .members-slider').lightSlider({
    item: 1,
    loop: true,
    easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    speed: 2000,
    pause: 4000,
    auto: true
});

$('body.consumers-page .members-slider').lightSlider({
    item: 5,
    loop: true,
    easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    speed: 2000,
    pause: 4000,
    auto: true,
    responsive : [{breakpoint: 1200, settings: {item: 4}},{breakpoint: 767, settings: {item: 3}},{breakpoint: 575, settings: {item: 2}}]
});


$b.on('click', '.xlSNext', function() {
    $(this).closest('section').find('.lSNext').trigger('click');
});

$b.on('click', '.xlSPrev', function() {
    $(this).closest('section').find('.lSPrev').trigger('click');
});

$('a.nav-item[href="' + location.pathname + '"]').addClass('active');

!function counters() {
    var formatNum = function(n){ return n.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '); };
    var $counters = $('[data-count-target]');
    var step = 0;
    var steps = 200;
    var int = setInterval(function() {
        step++;
        $counters.each(function(i, el) {
            var target = +el.dataset.countTarget;
            var next = Math.round(target/steps*step);
            if(next > target) next = target;
            el.innerText = formatNum(next);
        });
        if(+step >= steps) clearInterval(int);
    }, 10);
}();


$('[data-bg-color]')
    .on('mouseover', function() {
        this.style.borderColor = this.dataset.bgColor;
    })
    .on('mouseleave', function() {
        this.style.borderColor = '';
    });

$('.table-box-view-arrow').on('click', function () {
    $(this).parent().toggleClass('active')
        .next('.table-box-dropdown')
        .slideToggle(300);
});
$('.all-banks-view').on('click', function () {
    $(this).toggleClass('active')
        .next('.all-banks-dropdown')
        .slideToggle(300);
});

function copyToClipboard(str){
    var tmp   = document.createElement('INPUT');
    tmp.value = str;
    document.body.appendChild(tmp);
    tmp.select();
    document.execCommand('copy');
    document.body.removeChild(tmp);
    alert('Посилання скопійовано.');
}

$b.on('click', '.order-card-modal .modal-close-icon', function () {$('.order-card-modal').removeClass('show');});
$b.on('click', '.order-card-btn', function () {$('.order-card-modal').addClass('show');});
