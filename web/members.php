<?php
require_once 'php/lang.php';
require_once 'php/db.php';

?>
<!doctype html>
<html lang="<?= $lang ?>">
<?php require_once 'php/head.php' ?>
<body class="members-page">

<?php require_once 'php/header.php' ?>

<section class="banner">
    <div class="container">
        <h1><?= tr('Учасники') ?></h1>
    </div>
</section>

<section class="content">
    <div class="container">
        <h3>Банки які підключені до центрального маршрутизатора ПРОСТІР</h3>
        <div class="members-slider-box">
            <div class="members-slider">
                <div class="members-slider-item">
                    <?php foreach($members_1 as $item): ?>
                        <?php if($item): ?>
                            <a class="member-link" href="<?= $item['url'] ?>" target="_blank"
                               title="<?= $item['name'] ?>"
                               style="background-image:url(<?= $item['img'] ?>);" data-bg-color="<?= $item['color'] ?>"></a>
                        <?php else: ?>
                            <a class="member-link"></a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                <div class="members-slider-item">
                    <?php foreach($members_2 as $item): ?>
                        <?php if($item): ?>
                            <a class="member-link" href="<?= $item['url'] ?>"  target="_blank"
                               title="<?= $item['name'] ?>"
                               style="background-image:url(<?= $item['img'] ?>);" data-bg-color="<?= $item['color'] ?>"></a>
                        <?php else: ?>
                            <a class="member-link"></a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="xlSNext"></div>
            <div class="xlSPrev"></div>
        </div>
        <div class="all-banks">
            <div class="all-banks-view">
                <div class="all-banks-title">Всі банки</div>
                <div class="all-banks-arrow"></div>
            </div>
            <div class="all-banks-dropdown">
                <div class="members-slider-item">
                    <?php foreach($members_3 as $item): ?>
                        <a class="member-link" href="<?= $item['url'] ?>"  target="_blank"
                           title="<?= $item['name'] ?>"
                           style="background-image:url(<?= $item['img'] ?>);" data-bg-color="<?= $item['color'] ?>"></a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="table-box">
            <div class="table-box-view">
                <div class="table-box-view-title">Учасники НПС "ПРОСТІР"</div>
                <div class="table-box-view-arrow"></div>
            </div>
            <div class="table-box-dropdown"><?php require_once 'php/tables/members.php' ?></div>
        </div>
        <div class="table-box">
            <div class="table-box-view">
                <div class="table-box-view-title">Небанківські установи НПС "ПРОСТІР"</div>
                <div class="table-box-view-arrow"></div>
            </div>
            <div class="table-box-dropdown"><?php require_once 'php/tables/not-banks.php' ?></div>
        </div>
        <div class="table-box">
            <div class="table-box-view">
                <div class="table-box-view-title">Уповноважені виробники та персоналізатори карток НПС "ПРОСТІР"</div>
                <div class="table-box-view-arrow"></div>
            </div>
            <div class="table-box-dropdown"><?php require_once 'php/tables/producers.php' ?></div>
        </div>
        <div class="table-box">
            <div class="table-box-view">
                <div class="table-box-view-title">Оператори послуг платіжної інфраструктури НПС "ПРОСТІР"</div>
                <div class="table-box-view-arrow"></div>
            </div>
            <div class="table-box-dropdown"><?php require_once 'php/tables/operators.php' ?></div>
        </div>
    </div>
</section>

<?php require_once 'php/footer.php' ?>

</body>
</html>
