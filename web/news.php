<?php
require_once 'php/lang.php';
require_once 'php/db.php';

$page = (int)($_GET['page'] ?? 1);
$pages_count = 5;

?>
<!doctype html>
<html lang="<?= $lang ?>">
<?php require_once 'php/head.php' ?>
<body class="news-page">

<?php require_once 'php/header.php' ?>

<section class="banner">
    <div class="container">
        <h1><?= tr('Новини') ?></h1>
    </div>
</section>

<section class="content">
    <div class="container">
        <div class="news-box">
            <?php foreach($news as $item): ?>
                <div class="news-item-box">
                    <a class="news-item" href="/news-item.php?id=<?= $item['id'] ?>">
                        <div class="new-item-img" style="background-image:url(<?= $item['img'] ?>);"></div>
                        <div class="new-item-text-box">
                            <div class="new-item-date"><?= $item['date'] ?></div>
                            <div class="new-item-title"><?= $item['title'][$lang] ?></div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="pagination-box">
            <div class="pagination">
                <a <?= $page > 1 ? 'href="?page=' . ($page - 1) . '"' : '' ?> class="pagination-link prev"></a>
                <?php for($i = 1; $i <= $pages_count; $i++): ?>
                    <a <?= $i !== $page ? "href=\"?page=$i\"" : '' ?> class="pagination-link <?= $i === $page ? 'active' : '' ?>"><?= $i ?></a>
                <?php endfor; ?>
                <a <?= $page < $pages_count ? 'href="?page=' . ($page + 1) . '"' : '' ?> class="pagination-link next"></a>
            </div>
        </div>
    </div>
</section>

<?php require_once 'php/footer.php' ?>

</body>
</html>
